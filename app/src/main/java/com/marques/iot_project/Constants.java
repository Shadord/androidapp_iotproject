package com.marques.iot_project;

public class Constants {

    public static final String MQTT_BROKER_URL = "tcp://broker.hivemq.com:1883";

    public static final String PUBLISH_TOPIC_ALARM_CONTROL = "ProjetIOT/AlarmControl";
    public static final String SUBSCRIBE_TOPIC_TEMP = "ProjetIOT/Temp";
    public static final String SUBSCRIBE_TOPIC_HUMID = "ProjetIOT/Humid";
    public static final String SUBSCRIBE_TOPIC_ACCEL = "ProjetIOT/Accel";
    public static final String SUBSCRIBE_TOPIC_ALARM = "ProjetIOT/Alarm";
    public static final String SUBSCRIBE_TOPIC_ALARM_STATUS = "ProjetIOT/AlarmStatus";

    public static final String CLIENT_ID = "AndroidMobile";
}
