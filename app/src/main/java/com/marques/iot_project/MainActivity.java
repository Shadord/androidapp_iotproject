package com.marques.iot_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {


    public TextView card_temp_value, card_humid_value, card_x_value, card_y_value, card_z_value;
    public Switch switch_alarm;
    public CardView card_alarm, card_temp, card_humid, card_x, card_y, card_z;

    public TextView card_x_text, card_y_text, card_z_text;
    public ImageView card_temp_img, card_humid_img;

    private String TAG = "MqttService";


    public Context mContext;

    private Intent mServiceIntent;
    private MqttMessageService mqttMessageService;

    MqttReceiver receiver;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        receiver = new MqttReceiver();
        registerReceiver(receiver, new IntentFilter("GET_MQTT_DATA"));

        setupView();
        setupService();

    }

    public void setupService() {
        mqttMessageService = new MqttMessageService();
        mServiceIntent = new Intent(this, mqttMessageService.getClass());
        if (!isMyServiceRunning(mqttMessageService.getClass())) {
            startForegroundService(mServiceIntent);
        }
    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("MqttService status", "Running");
                return true;
            }
        }
        Log.i ("MqttService status", "Not running");
        return false;
    }

    class MqttReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("GET_MQTT_DATA")) {
                String topic = intent.getStringExtra("TOPIC");
                if(topic != null) {
                    double level = -1;
                    switch(topic) {
                        case "TEMPERATURE" : {
                            level = intent.getDoubleExtra(topic, 0.0);
                            setTempValue(level);
                            break;
                        }
                        case "HUMIDITY" : {
                            level = intent.getDoubleExtra(topic, 0.0);
                            setHumidValue(level);
                            break;
                        }
                        case "ACCEL" : {
                            String subtopic = intent.getStringExtra("SUBTOPIC");
                            if(subtopic != null) {
                                switch (subtopic) {
                                    case "X" : {
                                        level = intent.getDoubleExtra(topic, 0.0);
                                        setxValue(level);
                                        break;
                                    }
                                    case "Y" : {
                                        level = intent.getDoubleExtra(topic, 0.0);
                                        setyValue(level);
                                        break;
                                    }
                                    case "Z" : {
                                        level = intent.getDoubleExtra(topic, 0.0);
                                        setzValue(level);
                                        break;
                                    }
                                }

                            }
                            break;
                        }
                        case "ALARM_STATUS" :{
                            level = intent.getDoubleExtra(topic, 0.0);
                            Log.d(TAG, "level : " + level);
                            if(level == 1) {
                                switch_alarm.setChecked(true);
                            }else{
                                switch_alarm.setChecked(false);
                            }
                            break;
                        }
                        case "ALARM" : {
                            String subtopic = intent.getStringExtra("SUBTOPIC");
                            if(subtopic != null && switch_alarm.isChecked()) {
                                switch(subtopic){
                                    case "TEMP" : {
                                        setTempCardColorAlert();
                                        break;
                                    }
                                    case "HUMID" : {
                                        setHumidCardColorAlert();
                                        break;
                                    }
                                    case "ACCEL" : {
                                        setAxisCardColorAlert();
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                }




                // Show it in GraphView
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    private void sendDataToService(int message)
    {
        Intent sendLevel = new Intent();
        sendLevel.setAction("GET_MQTT_DATA_S");
        sendLevel.putExtra("ALARM_CONTROL", message);
        sendBroadcast(sendLevel);
    }





    private void setupView() {
        card_humid_value = findViewById(R.id.card_humid_value);
        card_temp_value = findViewById(R.id.card_temp_value);
        card_x_value = findViewById(R.id.card_x_value);
        card_y_value = findViewById(R.id.card_y_value);
        card_z_value = findViewById(R.id.card_z_value);

        card_x = findViewById(R.id.card_x);
        card_y = findViewById(R.id.card_y);
        card_z = findViewById(R.id.card_z);

        card_temp = findViewById(R.id.card_temp);
        card_humid = findViewById(R.id.card_humid);
        card_alarm = findViewById(R.id.card_alarm);


        card_temp_img = findViewById(R.id.card_temp_img);
        card_humid_img = findViewById(R.id.card_humid_img);
        card_x_text = findViewById(R.id.card_x_text);
        card_y_text = findViewById(R.id.card_y_text);
        card_z_text = findViewById(R.id.card_z_text);


        switch_alarm = findViewById(R.id.card_alarm_switch);

        switch_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(switch_alarm.isChecked()) {
                    sendDataToService(1);
                }else {
                    sendDataToService(0);
                    setColorNormal();
                }
            }
        });


    }





    public void setTempCardColorAlert() {
        card_temp.setCardBackgroundColor(getResources().getColor(R.color.red));
        card_temp_value.setTextColor(getResources().getColor(R.color.white));
        card_temp_img.setColorFilter(R.color.white);
        setAlarmCardColorAlert();
    }
    public void setHumidCardColorAlert() {
        card_humid.setCardBackgroundColor(getResources().getColor(R.color.red));
        card_humid_value.setTextColor(getResources().getColor(R.color.white));
        card_humid_img.setColorFilter(R.color.white);
        setAlarmCardColorAlert();
    }

    public void setAxisCardColorAlert() {
        card_x.setCardBackgroundColor(getResources().getColor(R.color.red));
        card_y.setCardBackgroundColor(getResources().getColor(R.color.red));
        card_z.setCardBackgroundColor(getResources().getColor(R.color.red));
        card_x_value.setTextColor(getResources().getColor(R.color.white));
        card_y_value.setTextColor(getResources().getColor(R.color.white));
        card_z_value.setTextColor(getResources().getColor(R.color.white));
        card_x_text.setTextColor(getResources().getColor(R.color.white));
        card_y_text.setTextColor(getResources().getColor(R.color.white));
        card_z_text.setTextColor(getResources().getColor(R.color.white));
        setAlarmCardColorAlert();
    }

    public void setAlarmCardColorAlert() {
        card_alarm.setCardBackgroundColor(getResources().getColor(R.color.red));
    }

    public void setColorNormal() {
        card_temp.setCardBackgroundColor(getResources().getColor(R.color.white));
        card_humid.setCardBackgroundColor(getResources().getColor(R.color.white));
        card_humid_value.setTextColor(getResources().getColor(R.color.gray));
        card_temp_value.setTextColor(getResources().getColor(R.color.gray));
        card_humid_img.setColorFilter(R.color.gray);
        card_temp_img.setColorFilter(R.color.gray);

        card_x.setCardBackgroundColor(getResources().getColor(R.color.white));
        card_y.setCardBackgroundColor(getResources().getColor(R.color.white));
        card_z.setCardBackgroundColor(getResources().getColor(R.color.white));
        card_x_value.setTextColor(getResources().getColor(R.color.gray));
        card_y_value.setTextColor(getResources().getColor(R.color.gray));
        card_z_value.setTextColor(getResources().getColor(R.color.gray));
        card_x_text.setTextColor(getResources().getColor(R.color.gray));
        card_y_text.setTextColor(getResources().getColor(R.color.gray));
        card_z_text.setTextColor(getResources().getColor(R.color.gray));

        card_alarm.setCardBackgroundColor(getResources().getColor(R.color.white));
    }

    public void setTempValue(double value) {
        card_temp_value.setText(value + " °C");
    }
    public void setHumidValue(double value) {
        card_humid_value.setText(value + " %");
    }
    public void setxValue(double value) {
        card_x_value.setText(value + " N");
    }
    public void setyValue(double value) {
        card_y_value.setText(value + " N");
    }
    public void setzValue(double value) {
        card_z_value.setText(value + " N");
    }

}