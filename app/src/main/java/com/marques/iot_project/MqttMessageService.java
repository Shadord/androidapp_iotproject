package com.marques.iot_project;


import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

/*import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;*/

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Semaphore;

import static com.marques.iot_project.App.CHANNEL_ID;

public class MqttMessageService extends Service {

    public static final String ACTION = "com.marques.iot_project.MqttMessageService";
    public MqttAndroidClient mqttAndroidClient;
    public String TAG = "MqttService";
    Intent intent;
    Semaphore s;
    Context mContext;
    MqttReceiver receiver;

    class MqttReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("GET_MQTT_DATA_S")) {

                int topic = intent.getIntExtra("ALARM_CONTROL", -1);
                Log.d(TAG, "alarmControl : " + topic);
                if (topic != -1) {

                    MqttMessage m = new MqttMessage();
                    String total = topic + "";
                    m.setPayload(total.getBytes());
                    publishAlarmControl(m);
                }
            }
        }
    }

    public MqttMessageService() {
    }


    @Override
    public void onCreate() {
        System.out.println("SERVICE LAUNCHED");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            startCustomForeground();
        else
            startForeground(1, new Notification());

        mContext = this;
        connect(mContext);
        receiver = new MqttReceiver();
        registerReceiver(receiver, new IntentFilter("GET_MQTT_DATA_S"));
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        intent = new Intent(ACTION);
        s = new Semaphore(1, true);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void connect(Context c) {
        String clientId = MqttClient.generateClientId();
        mqttAndroidClient = new MqttAndroidClient(c, Constants.MQTT_BROKER_URL, clientId);
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                Log.d(TAG, "onSuccessConnection");
                subscribeToTopic(Constants.SUBSCRIBE_TOPIC_TEMP);
                subscribeToTopic(Constants.SUBSCRIBE_TOPIC_HUMID);
                subscribeToTopic(Constants.SUBSCRIBE_TOPIC_ACCEL);
                subscribeToTopic(Constants.SUBSCRIBE_TOPIC_ALARM);
                subscribeToTopic(Constants.SUBSCRIBE_TOPIC_ALARM_STATUS);
            }

            @Override
            public void connectionLost(Throwable cause) {
                connect(mContext);
                Log.d(TAG, "onConnectionLost");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.d(TAG, "onMessageArrived " + topic);
                switch (topic) {
                    case Constants.SUBSCRIBE_TOPIC_TEMP: {
                        receivedTemp(message);
                        break;
                    }
                    case Constants.SUBSCRIBE_TOPIC_HUMID: {
                        receivedHumid(message);
                        break;
                    }
                    case Constants.SUBSCRIBE_TOPIC_ACCEL: {
                        receivedAccel(message);
                        break;
                    }
                    case Constants.SUBSCRIBE_TOPIC_ALARM: {
                        receivedAlarm(message);
                        break;
                    }
                    case Constants.SUBSCRIBE_TOPIC_ALARM_STATUS: {
                        receivedAlarmStatus(message);
                        break;
                    }
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                Log.d(TAG, "onDelivery");
            }
        });

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        try {
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.d(TAG, "onSuccess");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.d(TAG, "onFail");
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void subscribeToTopic(String topic) {
        try {
            mqttAndroidClient.subscribe(topic, 1, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.d(TAG, "TopicSubscribedSuccess " + topic);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.d(TAG, "TopicSubscribedFail " + topic);
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void receivedTemp(MqttMessage message) {
        String payload = new String(message.getPayload());
        double temp = 0;
        Log.d(TAG, "tempMessagePayload = " + payload);
        try {
            JSONObject jsonObject = new JSONObject(payload);
            temp = jsonObject.getDouble("temperature");
            sendDataToActivity("TEMPERATURE", null, temp);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void sendDataToActivity(String topic, String subTopic, double msg)
    {
        Intent sendLevel = new Intent();
        sendLevel.setAction("GET_MQTT_DATA");
        sendLevel.putExtra("TOPIC", topic);
        sendLevel.putExtra("SUBTOPIC", subTopic);
        sendLevel.putExtra( topic, msg);
        sendBroadcast(sendLevel);
    }

    public void receivedHumid(MqttMessage message) {
        String payload = new String(message.getPayload());
        double humid = 0;
        Log.d(TAG, "humidMessagePayload = " + payload);
        try {
            JSONObject jsonObject = new JSONObject(payload);
            humid = jsonObject.getDouble("humidity");
            sendDataToActivity("HUMIDITY", null, humid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void receivedAccel(MqttMessage message) {
        String payload = new String(message.getPayload());
        double x = 0;
        double y = 0;
        double z = 0;
        Log.d(TAG, "AccelMessagePayload = " + payload);
        try {
            JSONObject jsonObject = new JSONObject(payload);
            jsonObject = jsonObject.getJSONObject("accelerometre");
            Log.d(TAG, "AccelMessagePayload_sub = " + jsonObject.toString());
            x = jsonObject.getDouble("x");
            Log.d(TAG, "AccelMessagePayload_x = " + x);
            y = jsonObject.getDouble("y");
            z = jsonObject.getDouble("z");
            sendDataToActivity("ACCEL", "X", x);
            sendDataToActivity("ACCEL", "Y", y);
            sendDataToActivity("ACCEL", "Z", z);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void receivedAlarm(MqttMessage message) {
        String payload = new String(message.getPayload());
        String alarmOrigin = null;
        Log.d(TAG, "alarmMessagePayload = " + payload);
        try {
            JSONObject jsonObject = new JSONObject(payload);
            alarmOrigin = jsonObject.getString("alarmOrigin");
            sendDataToActivity("ALARM", alarmOrigin, 0);
        }catch (JSONException e) {
            e.printStackTrace();
        }
        setMessageNotification("ALARM", "Alarm déclenchée par : " + alarmOrigin, R.drawable.humid);
    }
    public void receivedAlarmStatus(MqttMessage message) {
        String payload = new String(message.getPayload());
        double alarmStatus = Double.valueOf(payload);
        sendDataToActivity("ALARM_STATUS", null, alarmStatus);
        Log.d(TAG, "alarmStatusPayload = " + payload);
    }

    public void publishAlarmControl(MqttMessage message) {
        try {
            Log.d(TAG, "alarmControlMessagePayload = " + message.getPayload());
            mqttAndroidClient.publish(Constants.PUBLISH_TOPIC_ALARM_CONTROL, message);
        }catch (MqttException e) {

        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void setMessageNotification(@NonNull String title, @NonNull String text, @NonNull int drawable) {
        Log.d(TAG, "sendNotification");
        int notifyID = 1983;

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent p = PendingIntent.getActivity(this, 0, notificationIntent, 0);

// Create a notification and set the notification channel.
        Notification notification = new Notification.Builder(this)
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(drawable)
                .setChannelId(CHANNEL_ID)
                .setColor(Color.BLUE)
                .setContentIntent(p)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
        notificationManager.notify(notifyID, notification);

    }

    private void startCustomForeground(){
        String NOTIFICATION_CHANNEL_ID = "Try";
        String channelName = "BackgroundService";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_DEFAULT);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        NotificationManager manager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setPriority(NotificationManager.IMPORTANCE_DEFAULT)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(2, notification);
    }


}

